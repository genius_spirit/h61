import React, { Component } from 'react';
import './About.css';
import axios from 'axios';
import Countries from "../components/CountriesList/CountriesList";
import CountryInfo from "../components/CountryInfo/CountryInfo";

const URL = 'https://restcountries.eu/rest/v2/';

class About extends Component {

  state = {
    countries: [],
    selectedCountryId: '',
    latlng: []
  };

  getCountryId = (id, latlng) => {
    this.setState({selectedCountryId: id, latlng: latlng});
  };

  componentDidMount() {
    axios.get(URL + 'all?fields=name;alpha3Code;flag;latlng').then(response => {
      this.setState({countries: response.data})
    }).catch(error => console.log(error));
  }

  render() {
    return(
      <div className='container'>
        <aside className='aside'>
          <Countries list={this.state.countries} showInfo={this.getCountryId}/>
        </aside>
        <main className='main'>
          <CountryInfo id={this.state.selectedCountryId} coord={this.state.latlng}/>
        </main>
      </div>
    )
  }
}

export default About;