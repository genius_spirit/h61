import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import axios from 'axios';
import './CountryInfo.css';

const URL = 'https://restcountries.eu/rest/v2/alpha/';

class CountryInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadedCountryData: null,
      borders: []
    };
  }

  componentDidUpdate() {
    if (this.props.id) {
      if (!this.state.loadedCountryData ||
        (this.state.loadedCountryData && this.state.loadedCountryData.alpha3Code !== this.props.id)) {
        axios.get(URL + this.props.id).then(response => {
          this.setState({loadedCountryData: response.data});
          Promise.all(response.data.borders.map(item => {
            return axios.get(URL + item).then(r => {
              return r.data.name
            });
          })).then(c => this.setState({borders: c}))
        })
      }
    }
  }

  render() {
    if (this.state.loadedCountryData) {
      return (
        <div className='flex-wrapper'>
          <div className='info'>
            <img src={this.state.loadedCountryData.flag}
                 alt="flag" width='120' height='80' border="1px solid #333"
                 style={{backgroundSize: 'cover'}}/>
            <h1>{this.state.loadedCountryData.name}</h1>
            <div>Capital: {this.state.loadedCountryData.capital}</div>
            <div>Population: {this.state.loadedCountryData.population}</div>
            <h3>Borders with:</h3>
            {this.state.borders.map((item, index) => {
              return <div key={index}>{item}</div>
            })}
          </div>
          <div className='map'>
            <GoogleMapReact
              bootstrapURLKeys={{key: 'AIzaSyCKNSTOkflOe89s2LF8hk2erR15tSuTt4I'}}
              center={{lat: this.props.coord[0], lng: this.props.coord[1]}}
              zoom={4}
            />
          </div>
        </div>
      )
    }
    return <p style={{textAlign: 'center'}}>Please select a country!</p>;
  }
}

export default CountryInfo;