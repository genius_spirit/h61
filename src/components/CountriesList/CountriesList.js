import React, {Fragment} from 'react';
import './CountriesList.css';

const Countries = ({list, showInfo}) => {
  return(
    <Fragment>
      {list.map(item => {
        return <div className='Countries__country-title'
                    onClick={() => showInfo(item.alpha3Code, item.latlng)}
                    key={item.alpha3Code}>
                <img src={item.flag} width='20' height='20' alt={item.alpha3Code}
                     style={{transform: "translateY(4px)", marginRight: '10px'}}/>
                  {item.name}
               </div>
      })}
    </Fragment>
  )
};

export default Countries;